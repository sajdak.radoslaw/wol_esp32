#include <Arduino.h>
#include <WiFi.h>

#define LED 15

const char *SSID = "TP-Link_2G";
const char *WiFiPassword = "";
String Password = "";
WiFiServer Server(1313);
WiFiClient RemoteClient;


void setup() {
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);

  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, WiFiPassword);
  Serial.print("Connecting to "); Serial.println(SSID);
 
  uint8_t i = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print('.');
    delay(500);
 
    if ((++i % 16) == 0)
    {
      Serial.println(F(" still trying to connect"));
    }
  }
 
  Serial.print(F("Connected. My IP address is: "));
  Serial.println(WiFi.localIP());
  Server.begin();
}

void EchoReceivedData()
{
  uint8_t ReceiveBuffer[30];
  while (RemoteClient.connected() && RemoteClient.available())
  {
    int Received = RemoteClient.read(ReceiveBuffer, sizeof(ReceiveBuffer));
    //RemoteClient.write(ReceiveBuffer, Received);
    
  }
}



void loop() {
  // put your main code here, to run repeatedly:
  if (Server.hasClient())
  {
    // If we are already connected to another computer, 
    // then reject the new connection. Otherwise accept
    // the connection. 
    if (RemoteClient.connected())
    {
      Serial.println("Connection rejected");
      Server.available().stop();
    }
    else
    {
      uint8_t ReceiveBuffer[32];
      Serial.println("Connection accepted");
      RemoteClient = Server.available();
      while (RemoteClient.connected())
      { 
        int received = RemoteClient.read(ReceiveBuffer, sizeof(ReceiveBuffer));
        for(int i = 0; i < received; i++) Serial.print((char)(ReceiveBuffer[i]));
        if(received > 0){
          if(received - 1 == Password.length()){
            Serial.println("Length test passed!");
            String tmpPassword;
            for(int j = 0; j < received - 1; j++) tmpPassword += (char)(ReceiveBuffer[j]);
            if(tmpPassword == Password)
            {
              Serial.println("Password test passed!");  
              digitalWrite(LED, HIGH);
              delay(200);
              digitalWrite(LED, LOW);
              
            }
            else{
              Serial.print(tmpPassword); Serial.print(" "); Serial.println(Password);
            }
          }
        }
      }
    }
  }
}

